#!/usr/bin/env python
from setuptools import setup

setup(
    name="tap-amplitude-api",
    version="0.1.9",
    description="Singer.io tap for extracting date from amplitude via API",
    author="FNM, ashalitkin",
    url="https://github.com/fridgenomore/singer-tap-amplitude ",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    py_modules=["tap_amplitude"],
    install_requires=[
        "singer-python==5.12.1",
        "requests==2.27.1",
        "pendulum==2.1.2"
    ],
    entry_points="""
    [console_scripts]
    tap-amplitude=tap_amplitude:main
    """,
    packages=["tap_amplitude"],
    package_data = {
        "schemas": ["tap_amplitude/schemas/*.json"]
    },
    include_package_data=True,
)
